import {TestBed} from '@angular/core/testing';
import {GaussianService} from './gaussian.service';

const inputMatrices = {
  /*
    5x + 3y = 6
    2x + 6y = 12
   */
  inputOne: {
    aMatrix: [
      [5, 3],
      [2, 6]
    ],
    vectorArray: [6, 12],
    result: [0, 2]
  },
  /*
    x + y + z = 6
    2x + y + 2z = 10
    x + 2y + 3z = 14
   */
  inputTwo: {
    aMatrix: [
      [1, 1, 1],
      [2, 1, 2],
      [1, 2, 3]
    ],
    vectorArray: [6, 10, 14],
    result: [1, 2, 3]
  }
};

let gaussianService: GaussianService;

describe('GaussianService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GaussianService
      ],
    });
    gaussianService = TestBed.get(GaussianService);
  });

  it('should be created', () => {
    const service: GaussianService = TestBed.get(GaussianService);
    expect(service).toBeTruthy();
  });

  it('should count result properly', () => {
    for (const indexer in inputMatrices) {
      if (inputMatrices.hasOwnProperty(indexer)) {
        const result = gaussianService.solve(inputMatrices[indexer].aMatrix, inputMatrices[indexer].vectorArray);
        expect(result).toEqual(inputMatrices[indexer].result);
      }
    }
  });

});
