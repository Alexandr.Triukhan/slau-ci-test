import { Injectable } from '@angular/core';

@Injectable()
export class GaussianService {

  constructor() { }

  private arrayFill(i, n, v) {
    const a = [];
    for (; i < n; i++) {
      a.push(v);
    }
    return a;
  }

  parseEquationString(equationString: string) {
    const equations = equationString.trim().split(/\r?\n/);
    const vectorArray = [];
    const aMatrix = [];
    equations.forEach( (equation) => {
      const equationArray = [];
      if (equation.length > 0) {
        const tmp = equation.split(/[+*=]/);
        for (let j = 0; j < tmp.length; j++) {
          if (j === tmp.length - 1) {
            vectorArray.push(parseInt(tmp[j], 10));
          } else {
            let aElement = tmp[j].replace(/\D/g, '');
            if (aElement.length === 0) {
              aElement = '1';
            }
            equationArray.push(parseInt(aElement, 10));
          }
        }
      }
      aMatrix.push(equationArray);
    });
    return [aMatrix, vectorArray];
  }

  /**
   * Gaussian elimination
   * @param  A matrix
   * @param  x vector
   * @return array x solution vector
   */
  solve(A, x) {

    let i, k, j;

    // Just make a single matrix
    for (i = 0; i < A.length; i++) {
      A[i].push(x[i]);
    }
    const n = A.length;

    for (i = 0; i < n; i++) {
      // Search for maximum in this column
      let maxEl = Math.abs(A[i][i]),
        maxRow = i;
      for (k = i + 1; k < n; k++) {
        if (Math.abs(A[k][i]) > maxEl) {
          maxEl = Math.abs(A[k][i]);
          maxRow = k;
        }
      }


      // Swap maximum row with current row (column by column)
      for (k = i; k < n + 1; k++) {
        const tmp = A[maxRow][k];
        A[maxRow][k] = A[i][k];
        A[i][k] = tmp;
      }

      // Make all rows below this one 0 in current column
      for (k = i + 1; k < n; k++) {
        const c = -A[k][i] / A[i][i];
        for (j = i; j < n + 1; j++) {
          if (i === j) {
            A[k][j] = 0;
          } else {
            A[k][j] += c * A[i][j];
          }
        }
      }
    }

    // Solve equation Ax=b for an upper triangular matrix A
    x = this.arrayFill(0, n, 0);
    for (i = n - 1; i > -1; i--) {
      x[i] = A[i][n] / A[i][i];
      for (k = i - 1; k > -1; k--) {
        A[k][n] -= A[k][i] * x[i];
      }
    }

    return x;
  }

}
