import { Component } from '@angular/core';
import { GaussianService} from './services/gaussian/gaussian.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  private equation: string;
  private result: string;

  constructor(private gaussianService: GaussianService) {}

  private solveEquation() {
    const [aMatrix, vectorArray] = this.gaussianService.parseEquationString(this.equation);
    this.result = this.gaussianService.solve(aMatrix, vectorArray);
  }

}
